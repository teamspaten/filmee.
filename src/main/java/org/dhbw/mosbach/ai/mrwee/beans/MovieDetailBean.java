package org.dhbw.mosbach.ai.mrwee.beans;

import org.dhbw.mosbach.ai.mrwee.dao.CommentDao;
import org.dhbw.mosbach.ai.mrwee.dao.MovieDao;
import org.dhbw.mosbach.ai.mrwee.dao.RatingDao;
import org.dhbw.mosbach.ai.mrwee.model.Comment;
import org.dhbw.mosbach.ai.mrwee.model.Movie;
import org.dhbw.mosbach.ai.mrwee.model.Rating;
import org.primefaces.event.RateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.OptionalDouble;
import java.util.Set;

/**
 * Used in movie detail view.
 */
@Named
@ViewScoped
public class MovieDetailBean implements Serializable {
	private static final Logger logger = LoggerFactory.getLogger(MovieDetailBean.class);
	private Movie selectedMovie;
	private String avgRating;
	private String currentRating;
	private Integer newRating;
	private String comment;

	@Inject
	private MovieDao movieDao;

	@Inject
	private RatingDao ratingDao;

	@Inject
	private CommentDao commentDao;

	@Inject
	private SecurityBean securityBean;

	/**
	 * Extracts the movie id from the request url.
	 */
	@PostConstruct
	private void init () {
		String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
		selectedMovie = movieDao.findById(Long.parseLong(id));
		Set<Rating> ratings = selectedMovie.getRatings();

		calcAvgRating(ratings);
		setCurrentRatingIfExist();
	}

	/**
	 * Calculates the average movie rating of the selected movie.
	 */
	private void calcAvgRating (Set<Rating> ratings) {
		OptionalDouble optionalDouble = ratings.stream().mapToInt(Rating::getRating).average();
		if (optionalDouble.isPresent()) {
			avgRating = String.valueOf(optionalDouble.getAsDouble());
		} else {
			avgRating = "bisher keine Bewertungen";
		}
	}

	/**
	 * Retrieves the rating of the current user for the selected movie.
	 */
	private void setCurrentRatingIfExist () {
		Rating rating = ratingDao.findByCreatorAndMovie(securityBean.getUser().getId(), selectedMovie.getId());
		if (rating != null) {
			this.currentRating = String.valueOf(rating.getRating());
		} else {
			this.currentRating = "keine Bewertung abgegeben";
		}
	}

	/**
	 * Called when the user applies a new rating to a movie. It replaces the last rating of the movie with the new rating.
	 */
	public void onRate (RateEvent event) {
		int rat = objToInt(event.getRating());

		if (rat < Rating.MIN_RATING || rat > Rating.MAX_RATING) {
			FacesContext.getCurrentInstance().addMessage("ratMsgs", new FacesMessage("Die Bewertung muss zwischen 1 und 10 Sterne haben!"));
			return;
		}

		Rating rating = new Rating(objToInt(event.getRating()), this.selectedMovie, securityBean.getUser());
		replaceRatingInSelectedMovie(rating);

		ratingDao.replace(rating);
		calcAvgRating(selectedMovie.getRatings());
		setCurrentRatingIfExist();
	}

	/**
	 * Called when a user comments on a movie.
	 */
	public void onComment () {
		Comment comment = new Comment(selectedMovie, securityBean.getUser(), this.comment);
		this.comment = "";

		commentDao.persist(comment);
		selectedMovie.getComments().add(comment);
	}

	/**
	 * Deletes a specific comment of a movie.
	 */
	public void deleteComment (Comment comment) {
		selectedMovie.getComments().remove(comment);
		commentDao.remove(comment);
	}

	/**
	 * Replaces the rating of the current user on the selected movie.
	 */
	private void replaceRatingInSelectedMovie (Rating newRating) {
		selectedMovie.getRatings().removeIf(rating -> rating.getCreator().getId() == securityBean.getUser().getId()
				&& rating.getMovie().getId() == selectedMovie.getId());
		selectedMovie.getRatings().add(newRating);

	}

	/**
	 * Converts an object to an integer.
	 */
	private int objToInt (Object obj) {
		if (obj != null && obj instanceof Integer) {
			return ((Integer) obj);
		} else {
			throw new ClassCastException("cannot cast " + obj.getClass() + " to int");
		}
	}

	public Movie getSelectedMovie () {
		logger.info("get " + selectedMovie.getTitle());
		return selectedMovie;
	}

	public void setSelectedMovie (Movie selectedMovie) {
		logger.info("set " + selectedMovie.getTitle());
		this.selectedMovie = selectedMovie;
	}

	public String getAvgRating () {
		return avgRating;
	}

	public void setAvgRating (String avgRating) {
		this.avgRating = avgRating;
	}

	public String getCurrentRating () {
		return currentRating;
	}

	public void setCurrentRating (String currentRating) {
		this.currentRating = currentRating;
	}

	public Integer getNewRating () {
		return newRating;
	}

	public void setNewRating (Integer newRating) {
		this.newRating = newRating;
	}

	public String getComment () {
		return comment;
	}

	public void setComment (String comment) {
		this.comment = comment;
	}

	/**
	 * Validates an user comment. A comment must contain 3 to 2048 characters.
	 */
	public void validateComment (FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value instanceof String) {
			String str = ((String) value);
			if (str.length() < 3 || str.length() > Comment.MAX_COMMENT_LENGTH) {
				throw new ValidatorException(new FacesMessage("Ein Kommentar muss zwischen 3 und 2048 Zeichen enthalten!"));
			}
		} else {
			throw new ValidatorException(new FacesMessage("unbekannter Fehler"));
		}
	}
}
