package org.dhbw.mosbach.ai.mrwee.beans;

import org.dhbw.mosbach.ai.mrwee.dao.MovieDao;
import org.dhbw.mosbach.ai.mrwee.model.Movie;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Used in MyMovies-View and AllMovies-View.
 */
@Named
@ViewScoped
public class ListMoviesBean implements Serializable {
	@Inject
	private MovieDao movieDao;

	@Inject
	private SecurityBean securityBean;

	private List<Movie> myMovies;
	private List<Movie> allMovies;

	@PostConstruct
	private void init () {
		this.myMovies = movieDao.getMoviesByOwner(securityBean.getUser());
		this.allMovies = movieDao.getAll();
	}

	public List<Movie> getMyMovies () {
		return myMovies;
	}

	public void setMyMovies (final List<Movie> myMovies) {
		this.myMovies = myMovies;
	}

	public List<Movie> getAllMovies () {
		return allMovies;
	}

	public void setAllMovies (final List<Movie> allMovies) {
		this.allMovies = allMovies;
	}

	public void deleteMovie (Movie movie) {
		movieDao.remove(movie);
	}
}