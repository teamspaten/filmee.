package org.dhbw.mosbach.ai.mrwee.dao;

import org.dhbw.mosbach.ai.mrwee.model.AppUser;
import org.dhbw.mosbach.ai.mrwee.model.Rating;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Contains database access methods for rating handling.
 */
public class RatingDao extends BaseDao<Rating> {

	/**
	 * Overwrites an existing rating or creates a new one.
	 */
	@Transactional
	public void replace (Rating rating) {
		removeExistingRating(rating.getCreator().getId(), rating.getMovie().getId());
		em.persist(rating);
	}

	/**
	 * Returns the specific rating of a given user and movie.
	 */
	public Rating findByCreatorAndMovie (long creatorId, long movieId) {
		Query query = em.createQuery("SELECT r FROM Rating r WHERE r.creator.id = :cid AND r.movie.id =:mid");
		query = query.setParameter("cid", creatorId);
		query = query.setParameter("mid", movieId);
		List list = query.getResultList();

		if (list.size() == 1 && list.get(0) instanceof Rating) {
			return (Rating) list.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Deletes a rating of a given user and movie.
	 */
	private void removeExistingRating (long creatorId, long movieId) {
		Rating toRemove = findByCreatorAndMovie(creatorId, movieId);
		if (toRemove != null) {
			em.remove(toRemove);
		}
	}

	/**
	 * Returns the last {@param count} ratings of all movies.
	 */
	public List<Rating> getLatest (int count) {
		Query query = em.createQuery("SELECT r FROM Rating r ORDER BY r.created DESC");
		return query.setMaxResults(count).getResultList();
	}

	/**
	 * Returns all ratings of a specific user.
	 */
	public List<Rating> getRatingsByOwner (AppUser user) {
		Query query = em.createQuery("SELECT r FROM Rating r WHERE r.creator.id = :uid ORDER BY r.created DESC");
		query = query.setParameter("uid", user.getId());
		return query.getResultList();
	}
}
