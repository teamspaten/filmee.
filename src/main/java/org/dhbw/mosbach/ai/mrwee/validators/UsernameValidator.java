package org.dhbw.mosbach.ai.mrwee.validators;

import org.dhbw.mosbach.ai.mrwee.dao.AppUserDao;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

/**
 * Author: karsten
 * Date:   29.05.2017.
 */
@FacesValidator("usernameValidator")
public class UsernameValidator implements Validator {

    @Inject
    private AppUserDao userDao;

    @Override
    public void validate(final FacesContext context, final UIComponent component, final Object value) throws ValidatorException {
        if (userDao.findByUnique("userName", value) != null) {
            FacesMessage msg = new FacesMessage("Username bereits vorhanden. Bitte wählen Sie einen anderen Namen");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }
}
