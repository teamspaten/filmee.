package org.dhbw.mosbach.ai.mrwee.startup;

import org.dhbw.mosbach.ai.mrwee.dao.AppUserDao;
import org.dhbw.mosbach.ai.mrwee.model.AppUser;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;

/**
 * Proxy class for creating some initial data.
 */
@Stateless(name = "appUserDaoProxy")
@SpecialAppUserDao
@PermitAll
public class AppUserDaoProxy extends AppUserDao {
	@PermitAll
	@Override
	public void changePassword (AppUser user, String password) {
		super.changePassword(user, password);
	}

	@PermitAll
	@Override
	public void persist (final AppUser entity) {
		super.persist(entity);
	}

	@PermitAll
	@Override
	public void persist (final AppUser... entities) {
		super.persist(entities);
	}

	@PermitAll
	@Override
	public AppUser findByUnique (final String fieldName, final Object key) {
		return super.findByUnique(fieldName, key);
	}
}
