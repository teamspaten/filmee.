package org.dhbw.mosbach.ai.mrwee.dao;

import com.google.common.reflect.TypeToken;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Contains basic database transactions.
 */
public abstract class BaseDao<T> implements Serializable {

	@PersistenceContext
	protected EntityManager em;
	protected final Class<?> entityClass;

	public BaseDao () {
		final TypeToken<T> type = new TypeToken<T>(getClass()) {};
		entityClass = type.getRawType();
	}

	/**
	 * Saves a single entity to the database.
	 */
	@Transactional
	public void persist (T entity) {
		em.persist(entity);
	}

	/**
	 * Saves multiple entities to the database.
	 */
	@Transactional
	public void persist (T... entities) {
		for (final T entity : entities) {
			persist(entity);
		}
	}

	/**
	 * Deletes an entity from the database.
	 */
	@Transactional
	public void remove (T entity) {
		entity = em.merge(entity);
		em.remove(entity);
	}

	/**
	 * Returns a specific entity with the given id.
	 */
	public T findById (Long id) {
		return (T) em.find(entityClass, id);
	}

	/**
	 * Returns a specific entity by a given unique key.
	 */
	public T findByUnique (String fieldName, Object key) {
		final String query = String.format("FROM %s e WHERE e.%s = :key", entityClass.getName(), fieldName);
		final List<T> result = em.createQuery(query).setParameter("key", key).getResultList();
		return result.isEmpty() ? null : result.get(0);
	}

	/**
	 * Returns all entities of a type.
	 */
	public List<T> getAll () {
		final String query = String.format("FROM %s e", entityClass.getName());
		return em.createQuery(query).getResultList();
	}
}
