package org.dhbw.mosbach.ai.mrwee.beans;

import org.dhbw.mosbach.ai.mrwee.model.AppRole;
import org.dhbw.mosbach.ai.mrwee.model.AppUser;
import org.dhbw.mosbach.ai.mrwee.startup.AppUserDaoProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.Size;

/**
 * Used for creating new user account.
 */
@Named
@RequestScoped
public class SignupBean {
	private static final Logger logger = LoggerFactory.getLogger(SignupBean.class);

	private String userName;
	private String password;
	private String name;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private AppUserDaoProxy appUserDaoProxy;

	public SignupBean () {}

	/**
	 * Creates a new user with the given credentials.
	 */
	public String signup () {
		logger.info("sign up new user " + userName + " with password " + password);
		final AppUser user = new AppUser(userName, name);
		user.getRoles().add(getUserRole());
		appUserDaoProxy.changePassword(user, password);
		appUserDaoProxy.persist(user);
		return "/signin.xhtml";
	}

	private AppRole getUserRole () {
		return em.createQuery("SELECT a FROM AppRole a WHERE a.name = 'user'", AppRole.class).getSingleResult();
	}

	public String getUserName () {
		return userName;
	}

	public void setUserName (String userName) {
		this.userName = userName;
	}

	@Size(min = 8, message = "Das Passwort muss mindestens 8 Zeichen lang sein.")
	public String getPassword () {
		return password;
	}

	public void setPassword (String password) {
		this.password = password;
	}

	public String getName () {
		return name;
	}

	public void setName (String name) {
		this.name = name;
	}

	/**
	 * Validates that the new user has a unique username.
	 */
	public void validateUniqueUsername (FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (appUserDaoProxy.findByUnique("userName", value) != null) {
			FacesMessage msg = new FacesMessage("Username bereits vorhanden. Bitte wählen Sie einen anderen Namen");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}