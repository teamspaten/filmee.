package org.dhbw.mosbach.ai.mrwee.beans;

import org.dhbw.mosbach.ai.mrwee.dao.AppUserDao;
import org.dhbw.mosbach.ai.mrwee.dao.CommentDao;
import org.dhbw.mosbach.ai.mrwee.dao.MovieDao;
import org.dhbw.mosbach.ai.mrwee.dao.RatingDao;
import org.dhbw.mosbach.ai.mrwee.model.AppUser;
import org.dhbw.mosbach.ai.mrwee.model.Comment;
import org.dhbw.mosbach.ai.mrwee.model.Movie;
import org.dhbw.mosbach.ai.mrwee.model.Rating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Used in user view.
 */
@Named
@ViewScoped
public class UserDetailBean implements Serializable {
	private static Logger logger = LoggerFactory.getLogger(UserDetailBean.class);

	@Inject
	private AppUserDao userDao;

	@Inject
	private MovieDao movieDao;

	@Inject
	private CommentDao commentDao;

	@Inject
	private RatingDao ratingDao;

	@Inject
	private SecurityBean securityBean;

	private boolean userFound;
	private String searchString;
	private AppUser user;
	private List<Movie> userMovies;
	private List<Rating> userRatings;
	private List<Comment> userComments;

	/**
	 * Updates the page with selected user's data.
	 */
	private void updateTables () {
		this.userComments = commentDao.getCommentsByOwner(user);
		this.userRatings = ratingDao.getRatingsByOwner(user);
		this.userMovies = movieDao.getMoviesByOwner(user);
	}

	/**
	 * Checks if the requested user exists.
	 */
	public void validateUsername (FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (userDao.findByUnique("userName", value) == null) {
			FacesMessage msg = new FacesMessage("Username nicht gefunden. Bitte wählen Sie einen anderen Namen");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}

	/**
	 * Finds the selected user.
	 */
	public String searchUser () {
		user = null;
		user = userDao.findByUnique("userName", searchString);
		if (user != null) {
			this.setUserFound(true);
			updateTables();
			return "Nutzer " + user.getName() + " gefunden.";
		} else {
			this.setUserFound(false);
			return "Nutzer konnte nicht gefunden werden.";
		}

	}

	public boolean isUserFound () {
		return userFound;
	}

	public void setUserFound (boolean userFound) {
		this.userFound = userFound;
	}

	public AppUser getUser () {
		return user;
	}

	public void setUser (AppUser user) {
		this.user = user;
	}

	public List<Movie> getUserMovies () {
		return userMovies;
	}

	public void setUserMovies (List<Movie> userMovies) {
		this.userMovies = userMovies;
	}

	public List<Rating> getUserRatings () {
		return userRatings;
	}

	public void setUserRatings (List<Rating> userRatings) {
		this.userRatings = userRatings;
	}

	public List<Comment> getUserComments () {
		return userComments;
	}

	public void setUserComments (List<Comment> userComments) {
		this.userComments = userComments;
	}

	public String getSearchString () {
		return searchString;
	}

	public void setSearchString (String searchString) {
		this.searchString = searchString;
	}

}
