package org.dhbw.mosbach.ai.mrwee.model;

/**
 * all existing user roles
 */
public class Roles {
	public static final String ADMIN = "admin";
	public static final String USER = "user";
}
