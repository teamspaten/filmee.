package org.dhbw.mosbach.ai.mrwee.beans;

import org.dhbw.mosbach.ai.mrwee.dao.AppUserDao;
import org.dhbw.mosbach.ai.mrwee.model.AppRole;
import org.dhbw.mosbach.ai.mrwee.model.AppUser;
import org.dhbw.mosbach.ai.mrwee.model.Roles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.Principal;
import java.util.Objects;
import java.util.Optional;

/**
 * General authentication bean. Contains the currently logged in user.
 */
@Named
@SessionScoped
public class SecurityBean implements Serializable {
	private static final Logger logger = LoggerFactory.getLogger(SecurityBean.class);

	@Inject
	private AppUserDao appUserDao;
	private AppUser loggedInUser;

	/**
	 * Returns the currently logged in user of a session.
	 */
	public AppUser getUser () {
		if (loggedInUser == null) {
			final Optional<Principal> principal = getPrincipal();
			if (principal.isPresent()) {
				loggedInUser = appUserDao.findByUnique("userName", principal.get().getName());
			} else {
				return null;
			}
		}
		return loggedInUser;
	}

	/**
	 * Checks if an user has admin rights.
	 */
	public boolean isAdmin () {
		for (AppRole appRole : loggedInUser.getRoles()) {
			if (Objects.equals(appRole.getName(), Roles.ADMIN)) {
				return true;
			}
		}
		return false;
	}

	private Optional<Principal> getPrincipal () {
		return Optional.ofNullable(getRequest().getUserPrincipal());
	}

	private HttpServletRequest getRequest () {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	/**
	 * Invalidates the active session and redirects the user to the login page.
	 */
	public String logout () {
		final HttpServletRequest request = getRequest();
		try {
			request.logout();
			request.getSession().invalidate();
		} catch (ServletException e) {
			logger.warn("exception on logout", e);
		} finally {
			loggedInUser = null;
		}
		return "/protected/user/index.xhtml?faces-redirect=true";
	}
}
