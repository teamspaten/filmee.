package org.dhbw.mosbach.ai.mrwee.dao;

import org.dhbw.mosbach.ai.mrwee.model.AppUser;
import org.dhbw.mosbach.ai.mrwee.model.Comment;

import javax.persistence.Query;
import java.util.List;

/**
 * Contains database access methods for comment handling.
 */
public class CommentDao extends BaseDao<Comment> {
	/**
	 * Return the last {@param count} comments of all movies.
	 */
	public List<Comment> getLatest (int count) {
		Query query = em.createQuery("SELECT c FROM Comment c ORDER BY c.created DESC");
		return query.setMaxResults(count).getResultList();
	}

	/**
	 * Returns all comment of a single user.
	 */
	public List<Comment> getCommentsByOwner (AppUser user) {
		Query query = em.createQuery("SELECT c FROM Comment c WHERE c.creator.id = :cid ORDER BY c.created DESC");
		query = query.setParameter("cid", user.getId());
		return query.getResultList();
	}
}
