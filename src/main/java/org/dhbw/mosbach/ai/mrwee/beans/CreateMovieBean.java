package org.dhbw.mosbach.ai.mrwee.beans;

import org.dhbw.mosbach.ai.mrwee.dao.MovieDao;
import org.dhbw.mosbach.ai.mrwee.model.Movie;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Used for creating a new movie.
 */
@Named
@RequestScoped
public class CreateMovieBean implements Serializable {
	private String title;
	private String description;
	private int releaseYear;

	@Inject
	private MovieDao movieDao;

	@Inject
	private SecurityBean securityBean;

	public String getTitle () {
		return title;
	}

	public void setTitle (String title) {
		this.title = title;
	}

	public String getDescription () {
		return description;
	}

	public void setDescription (String description) {
		this.description = description;
	}

	public int getReleaseYear () {
		return releaseYear;
	}

	public void setReleaseYear (int releaseYear) {
		this.releaseYear = releaseYear;
	}

	/**
	 * Called on submitting the movie creating form.
	 */
	public String createMovie () {
		Movie m = new Movie(title, description, releaseYear, securityBean.getUser());
		movieDao.persist(m);
		return "/protected/user/index.xhtml";
	}

	/**
	 * Validates a movie title. The title must contain 3 to 256 characters.
	 */
	public void validateTitle (FacesContext context, UIComponent component, Object value) throws ValidatorException {
		validateObjectAsString(3, Movie.MAX_TITLE_LENGTH, "Der Filmtitel muss zwischen 3 und 256 Zeichen lang sein!", value);
	}

	/**
	 * Validates a movie description. The description must contain 3 to 2048 characters.
	 */
	public void validateDescription (FacesContext context, UIComponent component, Object value) throws ValidatorException {
		validateObjectAsString(3, Movie.MAX_DESC_LENGTH, "Die Beschreibung des Film muss zwischen 3 und 2048 Zeichen lang sein!", value);
	}

	/**
	 * Validates a movie's release year. The release year must be between 1900 and the current year plus 5.
	 */
	public void validateReleaseYear (FacesContext context, UIComponent component, Object value) throws ValidatorException {
		validateObjectAsInteger(Movie.MIN_RELEASE_YEAR, Movie.MAX_RELEASE_YEAR, "Das Erscheinungsjahr des Films muss zwischen 1900 und " + Movie.MAX_RELEASE_YEAR + " liegen!", value);
	}

	/**
	 * Checks if a given object is a string. The amount of characters in the string must be in the given range.
	 */
	private void validateObjectAsString (int minLength, int maxLength, String errorMsg, Object value) throws ValidatorException {
		if (value instanceof String) {
			String str = ((String) value);
			if (str.length() < minLength || str.length() > maxLength) {
				throw new ValidatorException(new FacesMessage(errorMsg));
			}
		} else {
			throw new ValidatorException(new FacesMessage("unbekannter Fehler"));
		}
	}

	/**
	 * Checks if a given object is an integer. The integer value must be in the given range.
	 */
	private void validateObjectAsInteger (int min, int max, String errorMsg, Object value) throws ValidatorException {
		if (value instanceof Integer) {
			int i = ((int) value);
			if (i < min || i > max) {
				throw new ValidatorException(new FacesMessage(errorMsg));
			}
		} else {
			throw new ValidatorException(new FacesMessage("unbekannter Fehler"));
		}
	}
}
