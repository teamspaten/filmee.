package org.dhbw.mosbach.ai.mrwee.converter;

import org.dhbw.mosbach.ai.mrwee.dao.MovieDao;
import org.dhbw.mosbach.ai.mrwee.model.Movie;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Converts movies to strings and vice versa.
 */
@Named
@RequestScoped
public class MovieConverter implements Converter {

	@Inject
	private MovieDao movieDao;

	/**
	 * Returns the movie with the given id.
	 */
	@Override
	public Object getAsObject (FacesContext context, UIComponent component, String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}
		return movieDao.findById(Long.parseLong(value));
	}

	/**
	 * Returns the id of the given movie.
	 */
	@Override
	public String getAsString (FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		}

		if (value instanceof Movie) {
			Movie movie = ((Movie) value);
			return String.valueOf(movie.getId());
		} else {
			throw new ConverterException(String.format("cannot cast class %s to type Movie", value.getClass()));
		}

	}
}
