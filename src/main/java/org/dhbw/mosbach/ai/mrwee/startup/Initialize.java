package org.dhbw.mosbach.ai.mrwee.startup;

import org.dhbw.mosbach.ai.mrwee.dao.CommentDao;
import org.dhbw.mosbach.ai.mrwee.dao.MovieDao;
import org.dhbw.mosbach.ai.mrwee.dao.RatingDao;
import org.dhbw.mosbach.ai.mrwee.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.RunAs;
import javax.ejb.*;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Arrays;

/**
 * This class initializes the application with sample data.
 */
@Startup
@Singleton
@RunAs(Roles.ADMIN)
public class Initialize {
	private static final Logger logger = LoggerFactory.getLogger(Initialize.class);

	@PersistenceContext
	private EntityManager em;

	@Resource
	private TimerService timer;

	@EJB
	private AppUserDaoProxy appUserDaoProxy;

	@Inject
	private MovieDao movieDao;

	@Inject
	private CommentDao commentDao;

	@Inject
	private RatingDao ratingDao;

	private AppRole adminRole;
	private AppRole userRole;
	private AppUser adminUser;
	private AppUser userUser;
	private AppUser karstenUser;
	private AppUser kevinUser;
	private AppUser alexUser;
	private Movie fcMovie;
	private Movie kevinMovie;
	private Movie sherlockMovie;
	private Movie karstenMovie;
	private Movie entchenMovie;
	private Movie ckMovie;


	@PostConstruct
	private void initTimer () {
		timer.createSingleActionTimer(1000, new TimerConfig("init", false));
	}

	@Timeout
	private void init () {
		logger.info("Initialize.init");

		createRoles();
		createUsers();
		createMovies();
		createComments();
		createRatings();
	}

	/**
	 * create sample roles
	 */
	@Transactional(Transactional.TxType.REQUIRED)
	private void createRoles () {
		adminRole = new AppRole(Roles.ADMIN, "administrator");
		userRole = new AppRole(Roles.USER, "standard user");

		em.persist(adminRole);
		em.persist(userRole);
	}

	/**
	 * create sample users
	 */
	@Transactional(Transactional.TxType.REQUIRED)
	private void createUsers () {
		adminUser = createSingleUser("root", "Root User", "root", adminRole, userRole);
		userUser = createSingleUser("user", "Otto Normalbenutzer", "123", userRole);
		kevinUser = createSingleUser("kevin", "Kevin Lackmann", "kevin", userRole);
		alexUser = createSingleUser("alex", "Alexander Nicke", "alex", userRole);
		karstenUser = createSingleUser("karsten", "Karsten Köhler", "karsten", userRole);
	}

	/**
	 * create sample movies
	 */
	@Transactional(Transactional.TxType.REQUIRED)
	private void createMovies () {
		fcMovie = createSingleMovie("Fight Club", "Wir erfahren nicht einmal seinen Namen: der Erzähler dieser Geschichte (Edward Norton) bleibt anonym. Deprimiert und desillusioniert von der Leere seines Lebens in einer konsumorientierten Gesellschaft und einem geistabtötenden Job, besucht er Treffen von Krebskranken, wo er verschiedene Krankheiten vorgibt, um wahre Nähe und Anteilnahme parasitenhaft zu erfahren. Bis er zwei Personen kennenlernt, die sein Leben nachhaltig verändern werden: die kettenrauchende Marla Singer (Helena Bonham Carter), die seine Vorgehensweise kopiert und den Seifenvertreter Tyler Durden (Brad Pitt), der ihm eine neue Richtung vorgibt. Bei einer gegenseitigen Prügelei entdecken beide die befreiende Wirkung, das Beste im Kampf zu geben und die Narben hinterher mit Stolz zu tragen. Aus diesem Erlebnis erwächst der Fight Club, eine ganze Gruppe von Männern, die der gleichen brutalen Leidenschaft frönt. Und es werden immer mehr Teilnehmer und Clubs. Schließlich verselbständigt sich die Unternehmung und wird zu einer geheimen nationalen Bewegung mit terroristischen Zügen, zuviel für den Erzähler, doch nicht für Tyler Durden...", 1999, alexUser);
		entchenMovie = createSingleMovie("Der weiße Hai", "In dem auf einer Insel gelegenen kleinen Badeort Amity kommt es zu verschiedenen Todesfällen ungeklärter Herkunft. Der vom Festland stammende Polizeichef Brody (Roy Scheider) vermutet schon bald die Anwesenheit eines Hais, doch der Stadtrat und der Bürgermeister weigern sich wegen der bevorstehenden Touristensaison, die Strände sperren zu lassen. Als sich eine Bürgerjagd als erfolgreich erweist, scheint das Problem gelöst, doch der von Brody herbeigerufene Meeresbiologe Hooper (Richard Dreyfuss) prophezeit, daß der Terror weitergehen wird, denn es handelt sich um einen großen Weißen Hai, nicht um das von den Jägern gefangene Tier. Nachdem es fast zur Katastrophe gekommen ist, engagiert Brody den Haijäger und Seebären Quint (Robert Shaw), der mit Hooper und dem wasserscheuen Sheriff selbst auf dem kleinen Schiff \"Orca\" aufbricht, das mörderische Tier zu töten. Doch auf dem Meer hat der Hai Heimspiel...", 1975, userUser);
		kevinMovie = createSingleMovie("Avengers", "Loki (Tom Hiddleston) will Rache üben, weshalb er sich mit kriegerischen Aliens verbündet, die die Erde angreifen sollen. Um ihnen den Zugang zu verschaffen, benötigt er einen kosmischen Würfel, der über grenzenlose Energie verfügt. Dieser befindet sich im Hauptgebäude der Friedensorganisation S.H.I.E.L.D., wo er von dem Wissenschaftler Erik Selvig (Stellan Skarsgård) untersucht wird. Loki gelingt es, über den Würfel in das Gebäude einzudringen und ihn mit der Hilfe Selvigs und Clint Bartons, genannt Hawkeye (Jeremy Renner), die er zuvor umgepolt hatte, an sich zu bringen.\n\nDer Direktor von S.H.I.E.L.D., Nick Fury (Samuel L.Jackson), sieht sich gezwungen, ein ungewöhnliches Team zusammenzustellen, um Loki den Würfel wieder zu entwenden, noch nicht ahnend, was dieser tatsächlich damit vorhat. Gemeinsam mit der Agentin Natasha Romanow, genannt \"Black Widow\" (Scarlett Johansson), und seinem Mitarbeiter Agent Coulson (Clark Gregg), versucht er Iron Man (Robert Downey Jr.), Captain America (Chris Evans) und Dr.Bruce Banner, bekannt als \"Hulk\" (Mark Ruffalo), zusammen zubringen, was sich als sehr schwierig erweist, da ihre Gemeinsamkeiten eher gering sind. Dazu will auch Lokis Bruder Thor (Chris Hemsworth) noch ein Wörtchen mitsprechen...", 2012, kevinUser);
		sherlockMovie = createSingleMovie("Sherlock Holmes", "London um das Jahr 1890: Nach mehreren Ritualmorden gelingt es Holmes (Robert Downey Jr.) und Watson (Jude Law) endlich, den gefährlichen Lord Blackwood (Mark Strong) zu überführen, woraufhin dieser verhaftet und zum Tode durch den Strang verurteilt wird. Davon unbeeindruckt verkündet Blackwood, dass der Tod ihm nichts anhaben kann.\n" +
				"Und tatsächlich, auch nach der Hinrichtung des Lords gehen die Morde weiter und zwar exakt mit der Handschrift des hingerichteten Serienmörders. Selbst Watson, der sich nach der Vollstreckung selbst vom tatsächlichen Tod Blackwoods überzeugt hatte, ist sich plötzlich nicht mehr sicher, ob da alles noch mit rechten Dingen zugeht. Zudem beschäftigt ihn seine bevorstehende Heirat mit Mary Morstan (Kelly Reilly). Das alles bringt Holmes aber (kaum) aus der Ruhe. Logisch und analytisch setzt er seine Spürnase auf die Spuren der erneuten Morde an und dabei sind sein Verstand ebenso gefragt wie seine Schlagfertigkeiten.\n" +
				"Zusammen mit Watson gerät Holmes auf eine Hetzjagd quer durch London und in ein okkultes Abenteuer voller Verschwörungen und Intrigen...", 2009, adminUser);
		karstenMovie = createSingleMovie("Hackers", "Angeführt von dem einschlägig vorbestraften Hacker-Genie \"Zero Cool\", klinken sich eine handvoll jugendlicher Computerfreaks vorzugsweise in streng geheime Rechner von Konzernen, Banken oder der Regierung ein.", 1995, karstenUser);
		ckMovie = createSingleMovie("Citizen Kane", "Als der Medienzar Charles Foster Kane (Orson Welles) in seinem Traumschloß Xanadu stirbt, hinterläßt er ein Rätsel um sein letztes Wort: \"Rosebud\". Ein Journalist macht sich auf die Suche, um das Rätsel zu lösen, und spricht mit all den Leuten, die Kane durch sein Leben begleitet haben. Daraus ergibt sich das Bild eines ergeizigen Mannes, der zu Ruhm und Reichtum gekommen ist, aber dafür mit den Menschen brechen mußte und durch seine innere und gefühlsmäßige Verschlossenheit immer einsamer wurde, je mehr sein Alter voranschritt. Am Ende bleibt dem Journalisten die Lösung verwehrt, doch der Zuschauer weiß mehr...", 1941, alexUser);
	}

	/**
	 * create sample comments
	 */
	@Transactional(Transactional.TxType.REQUIRED)
	private void createComments () {
		createSingleComment(kevinMovie, kevinUser, "Bester Film den ich je gesehen habe");
		waitasecond();
		createSingleComment(kevinMovie, adminUser, "Kevin hat keine Ahnung von Filmen. Der war nicht so gut.");
		waitasecond();
		createSingleComment(entchenMovie, karstenUser, "\"Alle meine Entchen\nSchwimmen auf dem See\"");
		waitasecond();
		createSingleComment(karstenMovie, karstenUser, "Den fand ich nicht so gut.");
		createSingleComment(ckMovie, alexUser, "The Citizen Kane of movies");
		createSingleComment(sherlockMovie, alexUser, "Langweilig");
	}

	/**
	 * waits for a second.
	 */
	private void waitasecond () {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * create sample ratings.
	 */
	@Transactional(Transactional.TxType.REQUIRED)
	private void createRatings () {
		createSingleRating(10, kevinMovie, kevinUser);
		createSingleRating(9, entchenMovie, kevinUser);
		createSingleRating(2, kevinMovie, adminUser);
		createSingleRating(3, kevinMovie, userUser);
		createSingleRating(5, sherlockMovie, userUser);
		createSingleRating(10, karstenMovie, karstenUser);
		createSingleRating(10, fcMovie, karstenUser);
		createSingleRating(5, kevinMovie, karstenUser);
		createSingleRating(10, ckMovie, alexUser);
		createSingleRating(10, fcMovie, alexUser);
		createSingleRating(8, entchenMovie, alexUser);

	}

	/**
	 * creates a single user.
	 */
	private AppUser createSingleUser (String username, String name, String password, AppRole... roles) {
		logger.info("create user: " + username);
		final AppUser user = new AppUser(username, name);
		user.getRoles().addAll(Arrays.asList(roles));
		appUserDaoProxy.changePassword(user, password);
		appUserDaoProxy.persist(user);
		return user;
	}

	/**
	 * creates a single movie.
	 */
	private Movie createSingleMovie (String title, String desc, int year, AppUser creator) {
		final Movie movie = new Movie(title, desc, year, creator);
		movieDao.persist(movie);
		waitasecond();
		return movie;
	}

	/**
	 * creates a single comment
	 */
	private Comment createSingleComment (Movie movie, AppUser creator, String content) {
		final Comment comment = new Comment(movie, creator, content);
		commentDao.persist(comment);
		waitasecond();
		return comment;
	}

	/**
	 * creates a single rating.
	 */
	private Rating createSingleRating (int stars, Movie movie, AppUser creator) {
		final Rating rating = new Rating(stars, movie, creator);
		ratingDao.persist(rating);
		waitasecond();
		return rating;
	}
}
