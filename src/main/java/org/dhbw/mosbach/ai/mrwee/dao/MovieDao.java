package org.dhbw.mosbach.ai.mrwee.dao;

import org.dhbw.mosbach.ai.mrwee.model.AppUser;
import org.dhbw.mosbach.ai.mrwee.model.Movie;

import javax.persistence.Query;
import java.util.List;

/**
 * Contains database access methods for movie handling.
 */
public class MovieDao extends BaseDao<Movie> {
	/**
	 * Returns all movies of a single user.
	 */
	public List<Movie> getMoviesByOwner (AppUser user) {
		Query query = em.createQuery("SELECT m FROM AppUser a, Movie m WHERE a.userName=:user AND a.id = m.creator.id");
		return query.setParameter("user", user.getUserName()).getResultList();
	}

	/**
	 * Returns the last {@param count} created movies.
	 */
	public List<Movie> getLatest (int count) {
		Query query = em.createQuery("SELECT m FROM Movie m ORDER BY m.created DESC");
		return query.setMaxResults(count).getResultList();
	}
}

