package org.dhbw.mosbach.ai.mrwee.beans;

import org.dhbw.mosbach.ai.mrwee.dao.CommentDao;
import org.dhbw.mosbach.ai.mrwee.dao.MovieDao;
import org.dhbw.mosbach.ai.mrwee.dao.RatingDao;
import org.dhbw.mosbach.ai.mrwee.model.Comment;
import org.dhbw.mosbach.ai.mrwee.model.Movie;
import org.dhbw.mosbach.ai.mrwee.model.Rating;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Used in home screen for showing last user actions.
 */
@Named
@ViewScoped
public class HomeBean implements Serializable {
	@Inject
	private RatingDao ratingDao;

	@Inject
	private CommentDao commentDao;

	@Inject
	private MovieDao movieDao;

	private List<Comment> comments;
	private List<Rating> ratings;
	private List<Movie> movies;

	@PostConstruct
	private void init () {
		comments = commentDao.getLatest(5);
		ratings = ratingDao.getLatest(5);
		movies = movieDao.getLatest(5);
	}

	public List<Comment> getComments () {
		return comments;
	}

	public void setComments (List<Comment> comments) {
		this.comments = comments;
	}

	public List<Rating> getRatings () {
		return ratings;
	}

	public void setRatings (List<Rating> ratings) {
		this.ratings = ratings;
	}

	public List<Movie> getMovies () {
		return movies;
	}

	public void setMovies (List<Movie> movies) {
		this.movies = movies;
	}
}
