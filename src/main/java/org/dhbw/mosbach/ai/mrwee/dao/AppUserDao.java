package org.dhbw.mosbach.ai.mrwee.dao;

import org.dhbw.mosbach.ai.mrwee.model.AppUser;
import org.dhbw.mosbach.ai.mrwee.model.Roles;
import org.jboss.security.Base64Encoder;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Contains database access methods for user handling.
 */
@Named
@Dependent
@RolesAllowed(value = {Roles.ADMIN})
public class AppUserDao extends BaseDao<AppUser> {
	private MessageDigest getMessageDigest () {
		try {
			return MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Change the password of an user.
	 */
	@RolesAllowed(value = {Roles.ADMIN})
	public void changePassword (AppUser user, String password) {
		try {
			user.setPassword(Base64Encoder.encode(getMessageDigest().digest(password.getBytes())));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	@PermitAll
	public AppUser findByUnique (String fieldName, Object key) {
		return super.findByUnique(fieldName, key);
	}
}
